import { windi } from "svelte-windicss-preprocess";
import adapter from '@sveltejs/adapter-netlify';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: [
    windi({})
  ],
  kit: {
    // hydrate the <div id="svelte"> element in src/app.html
    adapter: adapter(),
    target: '#svelte'
  }
};

export default config;
